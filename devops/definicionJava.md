# Estandares backend

## Java

### Archivos:

El nombre de los archivos java será el mismo nombre de la clase de nivel mas alto de ese archivo.

### Líneas:

Mantener líneas con un limite de 100 caracteres. En caso de que la línea exceda ese limite escribirla en mínimo dos líneas con una tabulación de la línea inicial.

### Indentación:

Cada vez que se inicie un nuevo bloque de código este contara con un indentación de 4 caracteres.

### Estructura del archivo:

Un archivo tendrá principalmente esta estructura:

* Package: Nombre del paquete del archivo.
* Imports: Las importaciones que se usaran en el archivo
* Clase de alto nivel: Es la clase de mas alto nivel dentro del archivo

Todas serán separadas por una línea en blanco.

### Imports:

No se importaran paquetes completos, es decir: ```import java.util.*```
Las sentencias de import no serán restringidas por la cantidad de caracteres.
Las importaciones se harán en el siguiente orden:

* Imports estaticos
* Imports de clases dentro del proyecto
* Imports de clases de terceros

### Clases:

* Los nombres de las clases son en formato UpperCamelCase normalmente sustantivo o frase descriptiva.
* En caso de haber una sobrecarga de constructores o métodos no añadirse de forma desordenada, es decir agregarlos uno debajo del otro, en lo posible ordenarlos según la cantidad de parámetros.
* Los métodos y propiedades se escribirán en camelCase.
* Los métodos son verbos, ejemplo sendMessage().
* Las propiedades de la clase no deben ser publicas, para acceder a ellas tiene que ser con un getter/setter.
* Los parámetros se escribirán en camelCase, tienen que ser descriptivos, no se debe usar un solo carácter como nombre.
* Las variables locales se escriben en camelCase, en lo posible nombres descriptivos evitando un solo carácter, a excepción de variables de ciclo.
* Las anotaciones @... se escribirán en una línea propia.
* Los nombres de constantes usan _CONSTANT_CASE_.

### Estructuras de control:

Siempre se usaran las llaves de apertura y cierre {} para las estructuras de control así el bloque de código conste de una sola línea. Es importante la indentación del bloque de código de la estructura.
Dejar un carácter de espacio entre la estructura con la condición y entre la condición con la llave de apertura; es decir:

* Hacer ```if (isError) {```
* No hacer ```if(isError){``` 

