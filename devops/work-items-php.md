# ESTILOS DE CODIFICACIÓN - PHP

Este documento está basado en los estándares del grupo de interoperabilidad de PHP Framework, que se puede encontrar en la siguiente dirección [php-fig](https://www.php-fig.org/) donde tiene como objetivo principal hablar sobre los puntos en común entre nuestros proyectos y encontrar formas en las que podamos trabajar mejor juntos y todo lo concerniente a el como escribir código en el lenguaje en PHP haciendo uso de buenas prácticas de programación.

A continuación, se presenta el listado de los estándares que se deben manejar a la hora de escribir código en PHP

## 1. Archivos
- [Etiquetas PHP](https://www.php-fig.org/psr/psr-1/) -> Sección 2.1
- [Codificación de caracteres](https://www.php-fig.org/psr/psr-1/) -> Sección 2.2
## 2. Espacio de nombres y nombres de clases
- [Espacio de nombres y nombres de clases](https://www.php-fig.org/psr/psr-1/) -> Sección 3
## 3. Constantes de clase, propiedades y métodos
- [Constantes](https://www.php-fig.org/psr/psr-1/) -> Sección 4.1
- [Propiedades](https://www.php-fig.org/psr/psr-1/) -> Sección 4.2
- [Métodos](https://www.php-fig.org/psr/psr-1/) -> Sección 4.3
## 4. Líneas
- [Líneas](https://www.php-fig.org/psr/psr-12/) -> Sección 2.3
## 5. Sangría
- [Sangría](https://www.php-fig.org/psr/psr-12/) -> Sección 2.4
## 6. Palabras clave y tipos
- [Palabras clave y tipos](https://www.php-fig.org/psr/psr-12/) -> Sección 2.5
## 7. Declarar declaraciones, espacios de nombres e importar declaraciones
- [Declarar declaraciones, espacios de nombres e importar declaraciones](https://www.php-fig.org/psr/psr-12/) -> Sección 3
## 8. Clases, propiedades y métodos
- [Extiende e Implementa](https://www.php-fig.org/psr/psr-12/) -> Sección 4.1
- [Usando rasgos](https://www.php-fig.org/psr/psr-12/) -> Sección 4.2
- [Propiedades y constantes](https://www.php-fig.org/psr/psr-12/) -> Sección 4.3
- [Métodos y funciones](https://www.php-fig.org/psr/psr-12/) -> Sección 4.4
- [Argumentos de método y función](https://www.php-fig.org/psr/psr-12/) -> Sección 4.5
- [`abstract`, `final` y `static`](https://www.php-fig.org/psr/psr-12/) -> Sección 4.6
- [Llamadas a métodos y funciones](https://www.php-fig.org/psr/psr-12/) -> Sección 4.7
## 9. Estructuras de control
- [`if`, `elseif` y `else`](https://www.php-fig.org/psr/psr-12/) -> Sección 5.1
- [`switch`, `case`](https://www.php-fig.org/psr/psr-12/) -> Sección 5.2
- [`while`, `do while`](https://www.php-fig.org/psr/psr-12/) -> Sección 5.3
- [`for`](https://www.php-fig.org/psr/psr-12/) -> Sección 5.4
- [`foreach`](https://www.php-fig.org/psr/psr-12/) -> Sección 5.5
- [`try`, `catch`, `finally`](https://www.php-fig.org/psr/psr-12/) -> Sección 5.6
## 10. Operadores
- [Operadores unarios](https://www.php-fig.org/psr/psr-12/) -> Sección 6.1
- [Operadores binarios](https://www.php-fig.org/psr/psr-12/) -> Sección 6.2
- [Operadores ternarios](https://www.php-fig.org/psr/psr-12/) -> Sección 6.3
## 11. Cierres
- [Cierres](https://www.php-fig.org/psr/psr-12/) -> Sección 7
## 12. Clases anónimas
- [Clases anónimas](https://www.php-fig.org/psr/psr-12/) -> Sección 8
## 13. Autocarga
- [Especificación](https://www.php-fig.org/psr/psr-4/) -> Sección 2
- [Ejemplos](https://www.php-fig.org/psr/psr-4/) -> Sección 3
## 14. Interfaz de registrador
- [Especificación](https://www.php-fig.org/psr/psr-3/) -> Sección 1
    - [Conceptos básicos](https://www.php-fig.org/psr/psr-3/) -> Sección 1.1
    - [Mensaje](https://www.php-fig.org/psr/psr-3/) -> Sección 1.2
    - [Contexto](https://www.php-fig.org/psr/psr-3/) -> Sección 1.3
    - [Clases e interfaces auxiliares](https://www.php-fig.org/psr/psr-3/) -> Sección 1.4
- [Paquete](https://www.php-fig.org/psr/psr-3/) -> Sección 2
- [`Psr\Log\LoggerInterface`](https://www.php-fig.org/psr/psr-3/) -> Sección 3
- [`Psr\Log\LoggerAwareInterface`](https://www.php-fig.org/psr/psr-3/) -> Sección 4
- [`Psr\Log\LogLevel`](https://www.php-fig.org/psr/psr-3/) -> Sección 5
## 15. Interfaz de almacenamiento en caché
- [Interfaz de almacenamiento en caché](https://www.php-fig.org/psr/psr-6/) -> Toda la sección
## 16. Interfaz de contenedor
- [Especificación](https://www.php-fig.org/psr/psr-11/) -> Sección 1
    - [Identificadores de entrada](https://www.php-fig.org/psr/psr-11/) -> Sección 1.1.1
    - [Lectura de un contenedor](https://www.php-fig.org/psr/psr-11/) -> Sección 1.1.2
    - [Excepciones](https://www.php-fig.org/psr/psr-11/) -> Sección 1.2
    - [Uso recomendado](https://www.php-fig.org/psr/psr-11/) -> Sección 1.3
- [Paquete](https://www.php-fig.org/psr/psr-11/) -> Sección 2
- [Interfaces](https://www.php-fig.org/psr/psr-11/) -> Sección 3
## 17. Interfaces de definición de enlaces
- [Interfaces de definición de enlaces](https://www.php-fig.org/psr/psr-13/) -> Toda la sección
## 18. Despachador de eventos
- [Despachador de eventos](https://www.php-fig.org/psr/psr-14/) -> Toda la sección
## 19. Interfaz común para bibliotecas de almacenamiento en caché
- [Interfaz común para bibliotecas de almacenamiento en caché](https://www.php-fig.org/psr/psr-14/) -> Toda la sección