# ESTANDARES CODIFICACIÓN  - ANDROID

A continuación se describen los estandares utlizados para una buena codificació y comunicación en el area de moviles Android.

* Implementar ingles
Para incentivar el manejo de un 2 idioma y para realizar una buena comunicación se decide realizar todo proyecto en ingles.
* Anotacion tipo camello
* Uso de viewBinding no dataBinding
* Organizar variables del mismo nivel
* Organizar funciones vista, observers, unicas
* Importar solo lo que se necesite
* Procesamiento de datos solo en viewModel
* No realizar acciones en adaptadores
* Hacer commits de máximo 8 archivos
* No tener warnings
* No utilizar deprecados
* Utilización de clase sellada
* No dejar string ni dimens quemados
* Utilización de tools en el xml
* Relacionar layout de recyclerView
* Creación de nuevos proyectos en kotlin