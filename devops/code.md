# Codificación

> Siempre deja el código mejor de como lo encontraste

## **Estandarización**

El *wigicode* se debe basar en 3 características principales:

[1. **Legible**](#1-legible)
[2. **Mantenible**](#2-mantenible)
[3. **Testeable**](#3-testeable)
[$. **Seguro**](#4-seguro)

### 1. **legible**

- EditorConfig (*[link](https://editorconfig.org/)*):

  Cada proyecto debe tener en la raíz un archivo *.editorconfig* con  [*este contenido*](../.editorconfig) (recuerda acitvar/instalar el complemento en tu IDE preferido).

- Estructura y uso de llaves

  ```javascript
  //NO ❌

  //laves en otra línea
  if(a==b)
  {
  //algo();
  }

  //parámetros en otra línea
  function algo(
  param1,
  param2
  ){
  //algo()
  }

  //más de 3 parámetros por función
  function algo(param1,param2,param3,param4){
    algo();
  }

  //Usar comillas sencillas para textos
  var textoPrueba = 'algún texto';

  //Evitar el ;
  var otroTextoPrueba = "otro texto"

  //Declarar variables separadas por coma
  var dia="Lunes",edad=15;

  //Al validar un booleano colocar true/false
  if(esValido == true){
    //algo();
  }

  //No utilizar comparación estricta
  if(edad == 15){
    //edad="15" retorna true
    //algo();
  }

  //Hacer validaciones extra cuando el valor es falsy
  //https://developer.mozilla.org/en-US/docs/Glossary/Falsy
  if(dia !== ""){
    //algo();
  }
  ```

  ```javascript
  //SI ✔️

  //laves en la misma línea
  if(a===b) {
  //algo();
  }

  //parámetros en la misma línea
  function algo(param1,param2){
  //algo
  }

  //menos de 3 parámetros por función
  function algo(objParam){
    //objParam.param1;
    //objParam.param2;
    //objParam.param3;
    algo();
  }

  //Usar comillas dobles para textos
  var textoPrueba = "algún texto";

  //Colocar el ;
  var otroTextoPrueba = "otro texto"

  //Declarar variables de forma independeinte
  var dia="Lunes";
  var anio=2021;

  //Al validar un booleano obviar el true/false
  if(esValido){
    //algo();
  }

  //Utilizar comparación estricta
  if(edad === 15){
    //edad="15" retorna false
    //algo();
  }

  //No hacer validaciones extra cuando el valor es falsy
  //https://developer.mozilla.org/en-US/docs/Glossary/Falsy
  if(dia){
    //algo();
  }
  ```

- Guias de estilo
  - [html/css](https://google.github.io/styleguide/htmlcssguide.html)
  - [Angular](https://angular.io/guide/styleguide)
  - [php](https://gist.github.com/ryansechrest/8138375)
  - [Java](https://google.github.io/styleguide/javaguide.html)
  - [Android (Kotiln)](https://developer.android.com/kotlin/style-guide)
  - [IOS (swift)](https://google.github.io/swift/)
  - [IOS (Objective-c)](https://google.github.io/styleguide/objcguide.html)
  - [Shell](https://google.github.io/styleguide/shellguide.html)
  - [Python](https://google.github.io/styleguide/pyguide.html)

- Linters
  - [TypeScript](https://eslint.org/)

- Nombres de funciones/variables

  > Aunque el idioma estandar para programación es Inglés, la idea es que| el código sea entendible para los wigicoders, y por el contexto de los  proyectos/clientes que manejamos se usará el idioma español (sin tildes ni ñ) como estandar

  - Se usará camelCase
  - debe corresponder a su proposito
  - no ahorrar letras
  - el nombre debe ser tan claro que evite colocar comentarios
  - evitar "ñ", caractéres especiales o números.
  - la longitud recomendable es de 2 a 4 palabras o entre 8 y 20 caracteres

  ```javascript
  //NO ❌
  var PRIMERNUMERO=1;
  var SegundoNumero=2;
  var numero2=2;
  var tercer_numero=3;
  var nom="Pedro";
  var yy=2021;
  var año=2021;
  var USD$$=3000;
  var operación="suma";
  var oper="suma";

  function sumar(a1,a2,op){
    if(add){
      return a1 + a2;
    }else{
      a1 * a2;
    }
  }

  sumar(PRIMERNUMERO,SegundoNumero,oper);

  //
  ```

  ```javascript
  //SI ✔️
  var primerNumero=1;
  var segundoNumero=2;
  var tercerNumero=3;
  var nombre="Pedro";
  //ña=nia,ñe=>nie;...
  var anio=2021;
  var valorDolar=3000;
  var operacion="suma";

  function operacionMatematica(info){
    switch(info.operacion){
      case "suma":
        return a1 + a2;
      case "resta":
        return a1 - a2;
      default:
        return 0;
    }
  }

  infoOpreacion={
    "operacion":"suma",
    "primerNumero":primerNumero,
    "segundoNumero":segundoNumero
  };
  operacionMatematica(infoOpreacion);
  ```

### 2. **Mantenible**

- Código modular
- progrmación funcional
- Textos dinámicos (textos,urls,...)
- Principios SOLID (Alta Cohesión, bajo acoplamiento)
- Arquitectura (por plataforma)
  Android (Clean,)
  IOS (Viper)
  WEB (MVC)
- Patrones de diseño (por plataforma) (Antes de codificar realiza esa definicion entre los desarrolladores)
  - creación
  - estructurales
  - comportamiento
 //TODO: Definir que patrones se deben usar para los diferentes compoenentes generales
- Organización archivos/carpetas (por plataforma)

### 3. **Testeable**

- Unitarias
  (RED)
  (NEGOCIO) -> Criterios de aceptación / FUNCIONES / CLASES / METODOS
- Integración
- De Sistema
- Funcionales
- De carga
- De estres
- Aceptación

### 4. **Seguro**

La siguiente información plantea reglas de seguridad en la comunicación entre aplicaciones y backend

#### *OWASP*

Prevenir riegos como:

- Inyección
- Pérdida de Autenticación
- Exposición de datos sensibles
- Entidades Externas XML (XXE)
- Pérdida de Control de Acceso
- Configuración de Seguridad Incorrecta
- Secuencia de Comandos en Sitios Cruzados (XSS)
- Deserialización Insegura
- Componentes con vulnerabilidades conocidas
- Registro y Monitoreo Insuficientes
- Falsificación de peticiones en sitios cruzados CSRF
- Pérdida de autenticación y gestión de sesiones
- Referencia directa insegura a objetos
- Defectuosa configuración de seguridad
- Comunicaciones inseguras
- manipulación de URL
- entre otras

#### *mTLS*

![mtls](../img/code/mtls.png)

Se utiliza mTLS para asegurar la comunicación entre los dos actores, de esta forma se cubren varias técnicas de ataques a aplicaciones móviles:

***Soluciona***

- Man in the middle: ataques donde un atacante intercepta y modifica las tramas
- Replay attacks: ataques donde un atacante escucha las peticiones que se envían desde las aplicaciones
- Spoofing attacks: ataques de suplantación de identidad donde se obtienen credenciales de los usuarios
- Impersonation attacks: ataques de suplantación de identidad donde se apodera de sesiones del usuario

Se deben configurar todos los ambientes con mTLS para no tener ambientes vulnerables.

#### *Autenticación de dispositivos*

con este método cada dispositivo tiene su propio identificador y se puede reconocer en la plataforma, adicional el dispositivo se relaciona a un usuario y permite monitorear y cerrar sesiones si se detecta algún ataque desde un dispositivo en particular.

![authDevice](../img/code/authDevice.png)

***Soluciona***

- Peticiones desde ambientes no deseados
- control de dispositivos por usuario
- bloqueo de dispositivos por lotes, por usuarios, u otras condicione
- implementación de sesiones, control de sesiones por tiempo definido por negocio

#### *Variables de configuración*

No existirán variables de seguridad dentro de las aplicaciones, para obtener las variables de configuración se usara un Server Remote Config

![RemoteConfig](../img/code/remoteconfig.png)

Esto nos permite cambiar configuraciones en cualquier momento, habilitar y deshabilitar módulos, realizar configuraciones por dispositivo, sistema operativo o grupo de usuarios.

#### *Herramientas RASP*

![RASP](../img/code/RASP.png)

La autoprotección de aplicaciones en tiempo de ejecución (RASP) es una innovación en el ecosistema de seguridad de aplicaciones equipada para hacer frente a los ataques en tiempo de ejecución en la capa de aplicación del software al proporcionar más visibilidad de las vulnerabilidades ocultas. Es esencialmente un software de seguridad que se integra con una aplicación o su entorno de ejecución e intercepta constantemente las llamadas a la aplicación para verificar su seguridad. Un software RASP no espera a que una amenaza afecte a la aplicación. En cambio, busca de manera pro activa malware en el tráfico entrante a la aplicación y evita que se ejecuten llamadas fraudulentas dentro de la aplicación.

#### *Automatización*

Utilizar herramientas de prueba de seguridad para garantizar las siguientes puntos:

- Autenticación
- Autorización
- Confidencialidad
- Disponibilidad
- Integridad
- Resiliencia

## **Ambientes**

Cada ambiente se debe trabajar en contenedores (docker) en donde los diferentes ambientes sean copias fidedignas el uno del otro.

- Desarrollo (localhost)
- Desarrollo (compartido)
- Pruebas
- Producción

// Build variance

## **Documentacion**

- estructura commits
- changelog

## **Indicadores**

- % PR rechazados
- Code Coverage
- mantenibilidad

## **Flujo de Trabajo**

### Generar ramas iniciales

1. Se debe tomar de la rama master y pasarlo a la ***rama main*** (Va a ser la de producción)

```shell
git checkout master
##-b crea la rama
git checkout -b main
git pull origin master
```

2. En los ajustes del proyecto dejar la rama main como la default

3. Desde la rama main se crean las ramas ***qa*** y ***dev***

```shell
git checkout main
##-b crea la rama
git checkout -b qa
git pull origin main

git checkout -b dev
git pull origin main
```

### Desarrollar un nuevo módulo

1. Desde la rama ***dev*** se crea la rama feaure con el id de la tarea (Monday), dentro de esta rama van a estar el código y las pruebas unitarias, el nombre de la rama debe ser *hu_{{idTarea}}*, una vez finalizado el desarrollo se debe generar PR a la rama ***dev***

> El id de la tarea lo encuentras en la url https://wigilabs-squad.monday.com/boards/{{idCliente}}/pulses/{{idTarea}}

```shell
git checkout dev
##-b crea la rama
git checkout -b feature/hu_123
git pull origin dev
```

### Reporte de errores

> Si el error se reporta en la fase de pruebas (desarrollo (rama dev) - preproducción (rama qa)) no se genera nueva rama sino que se trabaja sobre la misma feature/hu_xxx

Una vez creada la tarea en el monday se crea la rama *hotfix/HF_{{idTicket}}* y desde esta se empieza a trabajar, una vez finalizado el ajuste se debe generar PR a la rama ***dev***

```shell
git checkout dev
##-b crea la rama
git checkout -b hotfix/HF_123
git pull origin dev
```

### Tags de PAP

[Volver](../readme.md#22-devops)
