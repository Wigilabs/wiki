# TS (TypeScript) 

- CamelCase nombre de clases, servicios , metodos (excepto nombre archivos)
Ej : Component Class(){}, globalService(){}

- Uso de guiones , guion bajo solo para nombrar los archivos .
- No usar numeracion ni caracteres especiales para cualquer tipo de nombre.
- Uso ortografía completa de palabras cuando sea posible.
Ej: UsuarioService.ts

- prefijos en nombre de paginas
  nombre - model de archivo - tipo

    1. services o servicios: consumo de los servicios, tratamien EJ: Global.service.ts , Usuario.service.ts
        Se crean por paginas o componentes , es decir por tantas paginas o componentes igual cantidad de servicios.
    2. pages o paginas [folder]: se crea una carpeta por pagina que contendra los archivos necesarios para hacer la visualizacion de los datos (controlador , archivo de estilo, maquetacion)
        EJ: usuario [folder]
            usuario.html 
            usuario.css
        carro [folder]
            carro.html
            carro.css
    3. shared o compartidos [folder] : archivos compartidos , usados en la logica de toda la pagina , puede ser css [folder] , pipes [folder] () , controladores [folder]
        Ej : shared [Folder]
                pages [folder]
                    header.css
                    footer.css
                css [folder]
                    botones.css
                    ... (necesarios de acuerdo a los componentes usados)
                    main.css (grid, estilos globales)
                    fonts.css
                pipes [folder]
                    numeracion.pipe.ts
                    (depende del desarrollo)

CODIGO : 
- Los textos que se van a mostrar deben estar localizados en un archivo aparte.
- Preferiblemente usar undefined a nulo.
- No use las declaraciones for..in (en desuso); en su lugar, use ts.forEach, ts.forEachKey y ts.forEachValue. Tenga en cuenta la diferencia entre ellos.
- Cambiar los bucles intente usar ts.forEach, ts.map y ts.filter.
- Usar prefereiblemente nuestras propias funciones (relacion con los pipes).
- Utilice siempre {} para encerrar el cuerpo del bucle y la declaración condicional.
- El comienzo {siempre está en la misma línea.

EJ: for (var i = 0, n = str.length; i < 10; i++) { },
if (i == c) { },
function f(x: number, y: string): void { }

- Cada declaración de declaración de variable declara solo una variable
EJ: use var x = 1; var y = 2; en lugar de var x = 1, y = 2;.

- Tratar de declarar el tipo o la clase a la cual hace referencia una variable , constante, etc, para esto hay que diferenciar las clases de las interfaces, pueden llegar a ser similares
adjunto https://desarrolloweb.com/articulos/clases-interfaces-servicios-angular.html

EJ: 
interface Cliente {
  nombre: String;
  cif: String;
  direccion: String;
  creado: Date;
}

let cliente: Cliente;

- Crear archivo independientes para manejar las intefaces de todo el proyecto (se puede someter a cambios), Ejemplo: 

--Interfaces [folder]
  |--ServicesInterfaces.ts
  |--VariablesIntefaces.ts
  |--Class[nombre clase]Interfaces.ts

- Constructor vs ngOnInit [anexo : https://medium.com/zurvin/cu%C3%A1l-es-la-diferencia-entre-ngoninit-y-constructor-en-angular-2f7ce3d986b7]

AUTENTICACION - TOKENIZACION , WEB TOKENS: pendiente

- Mensaje de error por defecto.
    NOTA : - La información rápida se divide en secciones generales. Si desea agregar un nuevo mensaje de solicitud, agregue 1 al código anterior como el nuevo código.
    1000 información de sintaxis
    2000 información del idioma
    4000 información de generación de instrucciones
    5000 información de la opción del compilador
    6000 información del compilador de línea de comando
    7000 noImplicitCualquier información


-- pages 
   |-- usuario [folder]
       |-- usuario.service.ts
       |-- usuario.controler.ts
       |-- usuario.html
   |-- login [folder]
       |-- login.service.ts
       |-- login.controler.ts
       |-- login.html
-- componentes
   |-- header [folder]
       |-- usuario.service.ts
       |-- usuario.controler.ts
       |-- usuario.html
   |-- footer [folder]
       |-- usuario.service.ts
       |-- usuario.controler.ts
       |-- usuario.html
-- shared [folder]
   |-- pages [folder]
       |-- header.css
       |-- footer.css
   |-- css [folder]
       |-- botones.css
       |-- main.css
       |-- fonts.css
   |-- pipes [folder]
       |-- numeracion.pipe.ts
   |-- Interfaces [folder]
       |-- nameInterface.ts
   |-- Clases [folder]
       |-- namemClass.ts
textos.json
README.md
index.html