# Analítica

En el proceso de analítica, inteligencia de negocio y segmentación de usuarios se intentan responder varias preguntas para poder ser asertivos en la definición de campañas enfocadas a los usuarios y en general para poder conocer al detalle que quiere nuestro usuario y como vamos a atacar su necesidad.
Las preguntas para resolver son:

- ¿Por qué?
- ¿Qué?
- ¿Cómo?
- ¿Cuándo?
- ¿Dónde?

Para la solución de estas preguntas participan varias áreas internas de Wigilabs, trabajando en conjunto con el cliente, para poder así encontrar el mejor camino a la resolución de las mismas:

- Cliente
- Business Analyst
- Data Science
- Arquitectura
- Big Data
- Desarrollo
- Machine Learning

Los pasos que implementa Wigilabs para el correcto desarrollo de métricas e inteligencia de negocio son los siguientes:

1. Conceptualización: en este punto se responde la pregunta “¿Por Qué?”. Es un trabajo conjunto entre el Cliente y el área de Business Analytics, en donde se definen los Insigths y KPIs con el fin de conocer, segmentar, predecir o lo que se desee, enfocado al flujo del negocio como al usuario final.
2. Definición: en este punto se responde la pregunta “¿Qué?”. El área de Data Science define qué datos son necesarios para poder cumplir con las conceptualizaciones del punto anterior.
3. Estructura: en este punto se responde la pregunta “¿Cómo?”, “¿Cuándo?” y “¿Dónde?”. Entre las áreas de Arquitectura y Big Data se define, en qué parte del flujo del proceso se obtendría la información definida, de qué forma se van a obtener estos datos, en donde y en qué estructura se debe almacenar esta información.
4. Desarrollo: el área de desarrollo realiza la ejecución del activo digital para obtener los datos y almacenarla de la forma establecida.
5. Despliegue: se realiza la publicación del activo desarrollado, y se testea en el público objetivo.
6. Procesamiento: el área de data science se encarga de analizar, limpiar y reprocesar la data recolectada con el fin de extraer las características principales, en este punto hay que evaluar si la solución está apuntando hacia el lado correcto, o se debe volver a la definición en el punto (2). Una vez se detecte que la solución está almacenando la información correcta, se realizan los dashboard informativos o reportes solicitados por el área del negocio.
7. Predicción: con la data limpia y pre-procesada, el área de Machine Learning se encarga de crear, diseñar y entrenar los modelos predictivos, que solucionen los insigths planteados.
8. Acción: una vez finalizado el ciclo, en conjunto entre el Cliente y el área de Business Analytics, se procede a evaluar la información presentada y con ella tomar acciones, como generar campañas publicitarias, o funcionalidades dentro algún activo de la compañía. Como este es un trabajo iterativo y constante, es recomendado, iniciar nuevamente este proceso una vez finalizado un ciclo, con el fin de cada vez conocer y cumplir con un máximo detalle las métricas definidas.
