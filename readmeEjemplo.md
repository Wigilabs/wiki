# **Nombre del proyecto**

```md
![logo del cliente](path/to/image.png)
```

***Tabla de contenido***

[[_TOC_]]

## **Descripción**

Pequeña descripción del proyecto

## **Status**

Lista de badges
//TODO: definir los badges a utilizar

## **Intalación y uso**

*Prerequisitos:*

- nodejs (^12.11.5)
- npm (^6.14.8)
- ...

*Herramientas:*

- Android Studio (^4.0)
- ...

*Despliegue:*

```bash
#pull from docker image
docker pull ubuntu:latest

#Iniciar localhost
docker run -p 1234
```

## **Desarrollo**

Una explicación breve de lo que se debería tener en cuenta en específico para el proyecto tanto en código como urls de diseños, servicios, en general un pequeño resumen

## **Contribuir**

<!--Siempre dejar el link al wiki de wigi para que se sigan los lineamientos generales de la empresa-->

Si hay algo especial a tener en cuenta para este proyecto especificarlo de lo contrario con el wiki general ya quedaría

> Para seguir los lineamientos generales para contribuir en los proyectos de ***[wigi](@wigilabs)*** recuerda ver el ***[wiki](https://gitlab.com/Wigilabs/wiki)***, y no olvides echar un vista a los estandares de ***[desarrollo](https://gitlab.com/Wigilabs/wiki/-/blob/main/devops/code.md#codificaci%C3%B3n)***

## **Módulos**

Lista de módulos, esto generalmente se debe actualizar por cada HU

### *Arquitectura*

Solo colocarlo si cambia en cuanti a los estandares generales de desarrollo

### *Patrones de diseño*

Solo colocarlo si cambia en cuanti a los estandares generales de desarrollo

## **Creditos**

Lista de las personas que han participado en el proyecto

- ***[Jorge](@jgaray.wigilabs)***
- ***[Elkin](@efracica.wigilabs)***
- ***[Sergio](@spenaranda.wigilabs)***
- ...
  
```md
***[nombre](@username_de_gitlab)***
```

## Changelog

//TODO: averiguar como hacer changelog automático
