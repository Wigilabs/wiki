# Proceso Ramas

## Proyecto existente

### Generar ramas iniciales

1. Se debe tomar de la rama master y pasarlo a la ***rama main*** (Va a ser la de producción)

```bash
git checkout master
##-b crea la rama
git checkout -b main
git pull origin master
```

2. En los ajustes del proyecto dejar la rama main como la default

3. Desde la rama main se crean las ramas ***qa*** y ***dev***

```bash
git checkout main
##-b crea la rama
git checkout -b qa
git pull origin main

git checkout -b dev
git pull origin main
```


### Desarrollar un nuevo módulo

1. Desde la rama ***dev*** se crea la carpeta feaure, dentro de esta carpeta una subcarpeta con el id de la tarea/HU (Monday) el nombre de la rama debe ser *hu_{{idTarea}}*, dentro de esta carpeta se va a generar una rama principal llamada ***code_{{nombreTarea}}*** donde van a estar el código y las pruebas unitarias (de esta rama es la que se va a generar el  PR a la rama ***dev***), de esta se pueden generar varias ramas a discreción del desarrollador.

> El id de la tarea lo encuentras en la url https://wigilabs-squad.monday.com/boards/{{idCliente}}/pulses/{{idTarea}}

```bash
git checkout dev
##-b crea la rama
git checkout -b feature/hu_123/login
git pull origin dev
```

**Estructura:**

<!-- prettier-ignore-start -->
<!-- Don't forget the two tabs! -->
    .
    ├── feauture                                    <- Carpeta por HU.
    │   └──hu_1190944458
    │           ├──login
    |           └──Ajuste-tilde
    ├── feauture-hu_1190944458-login                <- Rama por HU.
    └── feauture-hu_1190944458-ajuste-tilde

<!-- prettier-ignore-end -->


### Reporte de errores

> Si el error se reporta en la fase de pruebas (desarrollo (rama dev) - preproducción (rama qa)) no se genera nueva rama sino que se trabaja sobre la misma feature/hu_xxx


Una vez creada la tarea en el monday se crea la rama *hotfix/HF_{{idTicket}}* y desde esta se empieza a trabajar, una vez finalizado el ajuste se debe generar PR a la rama ***dev***

```bash
git checkout dev
##-b crea la rama
git checkout -b hotfix/HF_123
git pull origin dev
```

### Tags de PAP


