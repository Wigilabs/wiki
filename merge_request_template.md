# Creación de MR

## Descripción

(Ingresa una breve descripción de la funcionalidad que se va a integrar, ya sea un nuevo módulo o una corrección)

## Tipo de cambio

Por favor marque el cambio realizado.

- [ ] Bug fix (Cambio que soluciona algún problema).
- [X] New feature (Agrega una nueva funcionalidad).
- [ ] Breaking change (Cambio realizado que afecta una funcionalidad).
- [ ] Este cambio requiere actualizar la documentación del proyecto.

## Como probar lo realizado?

(Describe como poder realizar las pruebas para verificar lo realizado, indica cualquier detalle relevante)

* Inicio sesión
* Selección cuenta
* Botón detalle plan
* Prueba unitaria ....

## Infomación de la tarea
- [MN-001](https://wigilabs-squad.monday.com/boards/989742503/pulses/1330236868)

## A tener en cuenta:

* Mi código sigue las pautas de estilo de este proyecto.
* He realizado una autoevaluación de mi propio código.
* He comentado mi código, especialmente en áreas difíciles de entender.
* He realizado los cambios correspondientes a la documentación.--
* Mis cambios no generan nuevas advertencias.
* He agregado pruebas que indican que mi corrección es efectiva o que mi característica funciona.
* Las pruebas unitarias nuevas y existentes pasan localmente con mis cambios.
* He comprobado mi código y he corregido cualquier error ortográfico.

## ScreenShots
