# 🏠 **WIGILABS** <!-- omit in toc -->

## ***Bienvenido al wiki de wigi 💪*** <!-- omit in toc -->

En este documento se encontrarás el flujo de trabajo utilizado y un explicación de  como trabajamos en *wigilabs*

- [1. **Herramientas y organización**](#1-herramientas-y-organización)
  - [1.1. **Comunicación**](#11-comunicación)
    - [1.1.1. *Whatsapp*](#111-whatsapp)
    - [1.1.2. *Correo electrónico*](#112-correo-electrónico)
    - [1.1.3. *Microsoft Teams*](#113-microsoft-teams)
    - [1.1.4. *Microsoft Yammer*](#114-microsoft-yammer)
  - [1.2. **Gestión de Proyectos**](#12-gestión-de-proyectos)
    - [1.2.1. *Monday*](#121-monday)
    - [1.2.2. *Microsoft One Drive*](#122-microsoft-one-drive)
    - [1.2.3. *Microsotf Planner*](#123-microsotf-planner)
    - [1.2.4. *Jira*](#124-jira)
    - [*Azure Devops*](#azure-devops)
  - [1.3. **Desarrollo**](#13-desarrollo)
    - [1.3.1. *Gitlab*](#131-gitlab)
- [2. **Flujo de Trabajo**](#2-flujo-de-trabajo)
  - [2.1. **Preventa**](#21-preventa)
  - [2.2. **Devops**](#22-devops)
    - [2.2.1. *Desarrollo Continuo*](#221-desarrollo-continuo)
    - [2.2.2. *Pruebas Continuas*](#222-pruebas-continuas)
    - [2.2.3. *Integración Continua*](#223-integración-continua)
    - [2.2.4. *Despliegue Continuo*](#224-despliegue-continuo)
    - [2.2.5. *Monitoréo Continuo*](#225-monitoréo-continuo)

![wigi](img/wigi.jpg)

## 1. **Herramientas y organización**

Esta sección hace referencia a la organización de los clientes/proyectos en las diferentes herramientas que se utilizan en el día a día para llevar a cabo la consución de los proyectos.

### 1.1. **Comunicación**

En wigi utilizamos varias herramientas de comunicación interna:

#### 1.1.1. *Whatsapp*

Esta herramienta se usa princiaplmente por la inmediatez de la respuesta y debido a que varios clientes solicitan trabajar mediante esta, en wigi sabemos que en  la mayoría de veces el uso de la plataforma por parte de los wigicraks es de carcacter personal, por lo que, estamos en transición para dejar dejar de usar o disminuir el uso de esta para temas laborales.

#### 1.1.2. *Correo electrónico*

Esta herramienta se utiliza para ...

// TODO: Completar la documentación para el uso de correo

#### 1.1.3. *Microsoft Teams*

Esta herramienta se utiliza como el canal principal de comunicación interna, se utiliza para generar canales por cada cliente / proyecto y poder gestionar el tiempo (agendas) en el calendario que esta plataforma ofrece.

Se manejan 3 tipos de equipos:

- **wigi**: Este equipo se utiliza para la comunicación global interna de wigi, se maneja un canal ***general***, y uno por cada área.
- **{{cliente}}**: Se crea un equipo por cada cliente, se manejan dos canales principales por cliente ***general*** (este se comparte con el cliente) e ***interno***  y 2 canales similares adicionales por cada proyecto ***{{proyecto}}_general***, ***{{proyecto}}_interno***.

```mermaid
graph LR
    root[Teams] --> 1[wigi]
    root --> 2[ClienteX]
    root --> 3[Claro]
    subgraph 3g[Ejemplo --CLARO--]
    3 --> 31[General]
    3 --> 32[Interno]
    3 --> 33[MiClaro_General]
    3 --> 34[MiClaro_Interno]
    3 --> 35[CTA_General]
    3 --> 36[CTA_Interno]
    end
    subgraph 2g[Equipo ClienteX]
    2 --> 21[General]
    2 --> 22[Interno]
    2 --> 23[--ProyectoY--_General]
    2 --> 24[--ProyectoY--_Interno]
    2 --> 25[..]
    end
    subgraph 1g[Comunicación Interna]
    1 --> 11[General]
    1 --> 12[Desarrollo]
    1 --> 13[Proyectos]
    1 --> 14[RRHH]
    1 --> 15[...]
    end

linkStyle 0,1,2,3,4,5,6 stroke-width:1px;

style 1g fill:transparent,stroke:#777,stroke-width:1px,stroke-dasharray:5;
style 2g fill:transparent,stroke:#222,stroke-width:1px,stroke-dasharray:5;
style 3g fill:transparent,stroke:#f00,stroke-width:1px,stroke-dasharray:5;
style 3 fill:red,color:#fff;
```

***Ejemplo Cliente Claro:***
![Claro](img/tools/teams.png)

#### 1.1.4. *Microsoft Yammer*

Comunicación institucional

// TODO: Establecer como sería el uso por parte de RRHH

### 1.2. **Gestión de Proyectos**

En wigi utilizamos las siguientes herramientas para la gestión de proyectos:

#### 1.2.1. *Monday*

Esta herramienta se utiliza como el marco principal de cada proyecto, donde se realizan los asignamientos de tareas y el seguimiento a estas.

Los proyectos activos se manejan en un ***espacio de trabajo***, dentro de este espacio de trabajo se maneja una ***carpeta*** por cada cliente, dentro de esta carpeta se crea un ***tablero*** por cada proyecto, dentro cada tablero se crea un ***grupo de elementos*** por cada sprint, dentro de cada grupo se crea un ***elemento*** por cada historia de usuario, y dentro de cada elemento se crea un ***subelemento*** por cada criterio de aceptación  :

```mermaid
graph LR
    root[Monday ] --> 1[Proyectos Activos]
    root --> 2[Proyectos detenidos]
    subgraph 1ga[Espacio >> Carpeta ]
    1 --> 11[Cliente1]
    1 --> 12[...]
    end
    2 --> 21[Cliente1]
    2 --> 22[..]
    subgraph 1g[Tableros ]
    11 --> 111[Proyecto1]
    11 --> 112[...]
    end
    subgraph 2ga[grupos]
    111 --> 111g1[Sprint1]
    111 --> 111g2[...]
    end
    subgraph 3ga[elementos]
    111g1 --> 111g1a[HU1]
    111g1 --> 111g1b[...]
    end
    subgraph 4ga[.. >> subelemento]
    111g1a --> 111g1a1[Criterio de acept 1]
    111g1a --> 111g1a2[Criterio de acept 2]
    111g1a --> 111g1a3[..]
    end

linkStyle 0,1,2,3,4,5,6 stroke-width:1px;

style 1ga fiLL:#44444450,stroke:#777,stroke-width:1px,stroke-dasharray:5;
style 1g fill:#77777740,stroke:#777,stroke-width:1px,stroke-dasharray:5;
style 2ga fill:#99999930,stroke:#222,stroke-width:1px,stroke-dasharray:5;
style 3ga fill:#dddddd30,stroke:#222,stroke-width:1px,stroke-dasharray:5;
style 4ga fill:transparent,stroke:#222,stroke-width:1px,stroke-dasharray:5;
```

***Ejemplo CLiente Claro:***

![Monday CLaro](img/tools/monday.png)

>Si tienes dudas de como se realizan las estimaciones de los proyectos puedes echarle un vistaso a la [fase de planeación](devops/plan.md) dentro del ciclo de vida [devops](#22-devops)

#### 1.2.2. *Microsoft One Drive*

Se utiliza como repositorio general de todos los archivos correspondientes a los proyectos, la estructura de carpetas está conectada a los equipos de teams, en caso de que sean archivos que no se han transferido por teams debe tener la estructura ***cliente***/***proyecto***/***tema*** o ***sprint***:

```bash
──onedrive
    ├── Claro                           <- Carpeta por cliente
    │   ├── Mi Claro                    <- Carpeta por proyecto
    |   |   ├──Sprint 1                 <- Carpeta por Sprint
    |   |   |   |──Planeacion Sprint.xls
    |   |   |   └──Brief Sprint.ppt
    |   |   └──Estimaciones             <- Carpeta por tema
    |   |       |──Planeacion Estimar.xls
    |   |       └──Brief Estimar.ppt
    │   └── CTA                         <- Carpeta otro proyecto
    |      └── ...
    └── ...
```

#### 1.2.3. *Microsotf Planner*

//TODO: definir si se va a usar (Tiene integracipon cehvre con teams)

#### 1.2.4. *Jira*

//TODO: definir si se va ausar Jira

#### *Azure Devops*


El proceso admite los siguientes tipos de elemento de trabajo (WIT) para planear y realizar un seguimiento del trabajo, las pruebas, los comentarios y la revisión del código. Con diferentes Wit puede realizar un seguimiento de distintos tipos de trabajo, como — características, casos de usuario y tareas. Estos artefactos se crean al crear un proyecto mediante el proceso ágil. Se basan en principios y valores de Agile.

![Elementos de trabajo](img/plan/wits.png)

Además de Wit, los equipos tienen acceso a un conjunto de consultas de elementos de trabajo para realizar un seguimiento de la información, analizar el progreso y tomar decisiones.

[... continúa ...](https://docs.microsoft.com/es-es/azure/devops/boards/work-items/guidance/agile-process?view=azure-devops)

### 1.3. **Desarrollo**

En wigi utilizamos las siguientes herramientas para el desarrollo:

#### 1.3.1. *Gitlab*

Esta herramienta se utiliza como el repositorio principal para los proyectos.

***Usuarios***

Se debe crear un usuario creado con el correo {cuenta}@wigilabs.com y el *username* debe ser {cuenta}.wigilabs, el usuarios deben tener activada la validación de doble autenticación

***Organización***

Existe un grupo creado donde se ubica un subgrupo por cada cliente, dentro de este se crea un subgrupo por cada proyecto y adentro de este se crea un proyecto por cada plataforma a desarrollar, cada proyecto debe tener en el nombre el cliente, el proyecto y la plataforma manejado en minuscula ej (***claro_miclaro_android***,***progressus_agronomo_ios***,***claro_cta_back*** )

```mermaid
graph LR
    root[Gitlab ] --> 1[Wigilabs]
    subgraph 1ga[Grupo >> Subgrupo ]
      1 --> 11[Cliente1]
      1 --> 12[Cliente2]
      1 --> 13[...]
    end
    subgraph 1g[.. >> Subgrupo2 ]
      11 --> 111[Proyecto1]
      11 --> 112[Proyecto2]
      11 --> 113[...]
    end
    subgraph 2ga[.. >> Proyectos]
      111 --> 111g1[cliente1_proyecto1_android]
      111 --> 111g2[cliente1_proyecto1_ios]
      111 --> 111g3[cliente1_proyecto1_web]
      111 --> 111g4[...]
    end
    subgraph 2gb[.. >> Proyectos]
      112 --> 112g1[cliente1_proyecto2_back]
      112 --> 112g2[cliente1_proyecto2_web]
      112 --> 112g3[..]
    end

linkStyle 0,1,2,3,4,5,6 stroke-width:1px;

style 1ga fiLL:#44444450,stroke:#777,stroke-width:1px,stroke-dasharray:5;
style 1g fill:#77777740,stroke:#777,stroke-width:1px,stroke-dasharray:5;
style 2ga fill:#99999930,stroke:#222,stroke-width:1px,stroke-dasharray:5;
style 2gb fill:#99999930,stroke:#222,stroke-width:1px,stroke-dasharray:5;
```

***Roles***

Al grupo principal (*wigilabs*) todos los *wigicracks* van a tener acceso como rol *guest*, al *subgrupo del cliente* solo los colaboradores que vayan a participar en el cliente tendrán acceso con rol *reporter*, y dentro de cada proyecto los roles se van a manejar de la siguiente manera:

- *Maintainer:* La(s) gerente(s) del proyecto (PM) y la lider técnica del área correspondiente.
- *Developer:* La(s) desarrolladoras del proyecto.
- *Reporter:* Hereda los permisos del subgrupo del poryecto.

> Para más detalle de como funcionan los roles/permisos en gitlab [ver aquí](https://docs.gitlab.com/ee/user/permissions.html)

***Contenido Proyecto***

Cada Proyecto debe tener un archivo *readme.md* en la raiz con la siguiente estructura general (El readme puede variar por plataforma ej: ios, android, angular,php,...):

[Ejemplo readme.md](readmeEjemplo.md)

***Ramas***

- Cada proyecto debe tener una estructura de 6 ramas/carpetas principales, y cada rama debe estar protegida con la funcionalidad *protected branches*.

  - **main (*RAMA*)**

      En esta rama va a estar alojado el código que esté en producción, el paso de código a esta rama se realiza solo de la ***rama qa*** y este paso se da mediante PR/MR al ser exitosas las pruebas funcionales y de aceptación para las historias de usuario correspondientes

  - **qa (*RAMA*)**

      En esta rama va a estar alojado el código que está en la fase de pruebas (funcionales y aceptación), el paso de código a esta rama se realiza solo de la ***rama dev*** y este paso se da mediante PR/MR al ser exitosas las pruebas de sistema y de integración, así como las pruebas funcionales realizadas por cada desarrollador(es) para las historias de usuario correspondientes.

  - **dev (*RAMA*)**

      En esta rama va a estar alojado el código unificado de las historias de usuario que ya fueron desarrolladas, el paso de código a esta rama se realiza desde cada rama interna ubicada en la ***carpeta feature*** o en la ***carpeta hotfix*** y este paso se da al ser exitosas las pruebas unitarias para cada criterio de aceptación en las respectivas de usuario, así mismo como las pruebas de sistema y de integración, y también funcionales realizadas por cada desarrollador(es) para las historias de usuario correspondientes, aparte de ser aprobado trás una revisión de código por parte del TL y/o desarrolladores pares.

  - **feature (*CARPETA*)**

      Esta carpeta se usa para el desarrollo de cada funcionalidad, en esta carpeta van a estar alojadas inicialmente una carpeta por cada historia de usuario y adentro de esta 3 ramas, ***code, integrate*** y ***test,*** dentro de la ***rama feature/xxxx/code*** se desarrollarán las funcionalidades necesarias para la historia de usuario correspondiente y dentro de la ***rama feature/xxx/test*** se realizarán las pruebas unitarias necesarias para cumplir con cada criterio de aceptación de la historia de usuario correspondiente, una vez realizado el desarrollo y las pruebas unitarias se integran estas 2 ramas dentro de la ***rama feature/xxx/integrate*** y se deben ejecutar las pruebas correspondientes. Estás son las ramas básicas del proyecto y siempre que se inicie algún desarrollo se debe partir de lo que esté integrado en la ***rama dev.
      Ej Ramas:***  *feature/hu001/code ~ feature/hu001/test ~ feature/hu001/integrate*

  - **hotfix (CARPETA)**

      Esta carpeta se usa para la corrección de errores reportados en producción, en esta carpeta existirá una rama por cada error reportado nombrada según el ticket creado en la plataforma de ticket correspondiente en la que se reportó el error, una vez se realice la corrección de errores, debe superar las pruebas unitarias correspondientes y generar PR hacia la ***rama dev*** con todo lo que esto implica
      ***Ej Rama:** hotfix/1234*

  - **release (CARPETA)**

      Esta carpeta se usa para almacenar un histórico de los pasos a producción, en esta carpeta existirá una rama por cada versión exitosa publicada en producción, el paso de código a esta carpeta se realizará un tiempo (por definir) después del paso a producción. La rama va a estar nombrada basado en la fecha del pap
      ***Ej Rama:** release/v_20211215*

> Para más detalle sobre *protected branches* [ver aquí](https://docs.gitlab.com/ee/user/project/protected_branches.html)

> [Ejemplo template MR](merge_request_template.md)

## 2. **Flujo de Trabajo**

Esta sección hace referencia al flujo de trabajo iniciando desde la preventa, pasando por todo el ciclo de vida del proyecto, implementando la metodología/cultura DevOps:

### 2.1. **Preventa**

- Workshops
- Brief
- Propuesta Comercial
- Alcances Contractuales
- Épica

// TODO: Agregar información detallada del proceso en preventa

### 2.2. **Devops**

El proceso de desarrollo y despliegue se agrupa en 5 fases, con el fin de tener un flujo continuo de trabajo que integre varias áreas dentro del road map definido para poder ofrecer una mayor agilidad y calidad en el proceso completo que involucra el desarrollo, despliegue y soporte de los activos digitales, en cada fase se realizan tareas específicas en donde en el primer flujo, la mayoría son dependientes de las fases iniciales, pero entre más avance el proyecto se puede trabajar temas en paralelo según validaciones en conjunto Wigilabs-Cliente.

#### 2.2.1. *Desarrollo Continuo*

- ***[Planeación](devops/plan.md)***

En conjunto cliente/Wigilabs realiza la planeación de las tareas a ejecutar, basados en metodologías ágiles de desarrollo tales como Scrum, Kanban, Lean, etc. Estas metodologías son adaptativas y pueden variar adecuándose al cliente para poder avanzar con la mejor calidad y velocidad posible.

- ***[Codificación](devops/code.md)***

Las áreas de infraestructura, arquitectura y desarrollo se encargan de implementar las tareas acordadas en la planeación.

#### 2.2.2. *Pruebas Continuas*

- ***[Compilación Pruebas](devops/build.md#test)***

El área de QA se encarga de realizar pruebas unitarias para cada punto a desarrollar cuando se realiza la compilación de los diferentes activos por parte del área de desarrollo, este punto garantiza calidad y cumplimiento de estándares en el desarrollo. Estas pruebas deben ser exitosas, de lo contrario el módulo vuelve al punto de codificación.

- ***Pruebas***

El área de QA de Wigilabs, junto al área de pruebas del cliente realizan pruebas funcionales dentro de cada producto, para poder garantizar la calidad de cada uno de estos, estas pruebas deben ser exitosas, de lo contrario el módulo vuelve al punto de codificación.

#### 2.2.3. *Integración Continua*

- ***[Compilación Integración](devops/build.md#integrate)***

Una vez sean exitosas todas las pruebas, se realizan las diferentes complicaciones para los diferentes ambientes y así poder realizar los despliegues en producción.

#### 2.2.4. *Despliegue Continuo*

- ***Despliegue***

Se realiza el despliegue del ambiente de pruebas al ambiente productivo, de los diferentes desarrollos que se vean involucrados con el módulo en ejecución.

- ***Publicación***

Se realiza la publicación en las diferentes tiendas de aplicaciones, según cada sistema operativo.

#### 2.2.5. *Monitoréo Continuo*

- ***Monitoréo***

una vez puesto en producción se realiza monitoreo preventivo / reactivo de las soluciones implementadas, para poder reaccionar de forma temprana ante cualquier eventualidad presentada en cualquier punto de Infraestructura o desarrollo.

- ***[Analítica](devops/analitycs.md)***

En conjunto con el cliente se definen métricas y desarrollo de metodologías en pro del mejoramiento continuo de cada uno de los activos implementados.

